# Elevator Buttons

## Description

This mod adds a variety of numbered buttons and ringbells to the Wiring Station to satisfy your elevator needs.

## Compatibility

- Compatible with Starbound 1.4
- Compatible with Frackin' Universe

## Steam Workshop Links

- Elevator Buttons: https://steamcommunity.com/sharedfiles/filedetails/?id=736693211

## Credits

- Me, CC-BY-4.0