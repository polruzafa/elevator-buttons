function init()
  self.goldenbellSoundTimer = 0
  self.goldenbellSoundDuration = config.getParameter("goldenbellSoundDuration")
end

function update(dt)
  if not object.isInputNodeConnected(0) or object.getInputNodeLevel(0) then
    animator.setAnimationState("goldenbellState", "on")
    if self.goldenbellSoundTimer <= 0 then
      animator.playSound("goldenbell")
      self.goldenbellSoundTimer = self.goldenbellSoundDuration
    else
      self.goldenbellSoundTimer = self.goldenbellSoundTimer - dt
    end
  else
    self.goldenbellSoundTimer = 0
    animator.setAnimationState("goldenbellState", "off")
  end
end
