function init()
  self.sounds = config.getParameter("sounds", {})
  animator.setSoundPool("noise", self.sounds)
  object.setInteractive(true)

  if storage.timer == nil then
    storage.timer = 0
  end
  self.interval = config.getParameter("interval")
end

function onInteraction()
  if #self.sounds > 0 then
    animator.playSound("noise");
    animator.setAnimationState("switchState", "on")
    storage.timer = self.interval
  end
end

function onNpcPlay(npcId)
  local interact = config.getParameter("npcToy.interactOnNpcPlayStart")
  if interact == nil or interact ~= false then
    onInteraction()
  end
end

function update(dt)
  if storage.timer > 0 then
    storage.timer = storage.timer - 1
    if storage.timer == 0 then
      animator.setAnimationState("switchState", "off")
    end
  end
end
