function init()
  self.bellSoundTimer = 0
  self.bellSoundDuration = config.getParameter("bellSoundDuration")
end

function update(dt)
  if not object.isInputNodeConnected(0) or object.getInputNodeLevel(0) then
    animator.setAnimationState("bellState", "on")
    if self.bellSoundTimer <= 0 then
      animator.playSound("bell")
      self.bellSoundTimer = self.bellSoundDuration
    else
      self.bellSoundTimer = self.bellSoundTimer - dt
    end
  else
    self.bellSoundTimer = 0
    animator.setAnimationState("bellState", "off")
  end
end
